#!/bin/sh

arg="$1"
if [ $arg == "setup" ]; then
	python -m flask run
elif [ $arg == "debug" ]; then
	exec /bin/sh
else
	exec "$@"
fi