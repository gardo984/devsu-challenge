pipeline {
	agent any
	environment {
		REGISTRY_URL = credentials('DOCKER_HUB_REGISTRY_URL')
		IMAGE_NAME = 'malazo/devsu-backend'
		IMAGE_TAG = "${IMAGE_NAME}:${env.BUILD_ID}"
	}
	stages {
		stage('Build') {
			agent any
			steps {
				script {
					sh "docker build -f backend/Dockerfile --build-arg='UID=1000' -t ${IMAGE_TAG} ."
				}
			}
		}
		stage('Tests') {
			agent any
			steps {
				script {
					def redis_name = "devsu_broker_${env.BUILD_ID}"
					docker.image('redis:alpine').withRun("--name ${redis_name}"){ cr ->
						def backend_name = "devsu_backend_${env.BUILD_ID}"
						def backend_image = docker.image("${IMAGE_TAG}")
						backend_image.withRun("--name ${backend_name} --link ${redis_name}:devsu_broker --env-file=config/.backend.env") { cb ->
							sh "docker exec ${backend_name} python -m pytest -v"
						}
					}
				}
			}
		}
		stage('Publish') {
			agent any
			steps {
				script {
					docker.withRegistry("${REGISTRY_URL}","DOCKER_HUB_CREDENTIALS") {
						def builtImage = docker.image("${IMAGE_TAG}")
						builtImage.push()
						builtImage.push('latest')
				    }
				}
			}
		}
		stage('Deploy'){
			agent {
				docker {
					image 'malazo/alpine-kubectl:latest'
					args '-u root'
				}
			}
			steps {
				script {
					withCredentials([file(credentialsId: 'KUBE_CONFIG_KNODE3', variable: 'kube_config')]) {
						sh "cp \$kube_config /root/.kube/config"
						sh 'kubectl apply -f k8s/deploy-broker.yaml'
						sh 'kubectl apply -f k8s/svc-broker.yaml'
						sh 'kubectl apply -f k8s/cm-backend.yaml'
						sh 'kubectl apply -f k8s/deploy-backend.yaml'
						sh 'kubectl apply -f k8s/svc-backend.yaml'
						// sh 'kubectl apply -f k8s/ingress-backend.yaml'
					}
				}
			}
		}
	}
	post {
		success {
			sh 'echo "Application has been delivered"'
			deleteDir()
		}
		failure {
			deleteDir()	
		}
	}
}
