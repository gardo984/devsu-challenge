import pytest
from typing import (
    Dict,
    Mapping,
)

SECRET_KEY: str = "2f5ae96c-b558-4c7b-a590-a501ae1c3f6c"
DATA_CREDENTIALS: Dict = {"username": "devsu", "password": "test", }
DATA_PAYLOAD: Dict = {
    "message": "This is a test", "to": "Juan Perez",
    "from": "Rita Asturia", "timeToLifeSec": 45,
}
DATA_HEADERS: Dict = {
    "X-JWT-KWY": None,
    "X-Parse-REST-API-Key": SECRET_KEY,
}


@pytest.mark.parametrize(
    'credentials', [DATA_CREDENTIALS, ]
)
def test_auth_endpoint(client, credentials: Mapping):
    rsp = client.post('/auth/login', json=credentials)
    assert rsp.status_code == 200
    assert isinstance(rsp.json, Dict)
    assert 'access_token' in rsp.json
    assert rsp.json.get('access_token') is not None


@pytest.mark.parametrize(
    'credentials,payload,headers_template',
    [(DATA_CREDENTIALS, DATA_PAYLOAD, DATA_HEADERS,)]
)
def test_devops_endpoint(
    client,
    credentials: Mapping,
    payload: Mapping,
    headers_template: Mapping
):
    rsp = client.post('/auth/login', json=credentials)
    headers = headers_template.copy()
    headers['X-JWT-KWY'] = rsp.json.get('access_token')
    outcome = client.post(
        '/DevOps', json=payload, headers=headers
    )
    assert outcome.status_code == 200
    assert isinstance(outcome.json, Dict)
    assert 'message' in outcome.json
    assert outcome.json.get('message') is not None


@pytest.mark.parametrize(
    'credentials,payload,headers_template',
    [(DATA_CREDENTIALS, DATA_PAYLOAD, DATA_HEADERS,)]
)
def test_devops_endpoint_invalid_secret(
    client,
    credentials: Mapping,
    payload: Mapping,
    headers_template: Mapping
):
    rsp = client.post('/auth/login', json=credentials)
    headers = headers_template.copy()
    headers['X-JWT-KWY'] = rsp.json.get('access_token')
    headers['X-Parse-REST-API-Key'] = 'invalid-value'
    outcome = client.post(
        '/DevOps', json=payload, headers=headers
    )
    assert outcome.status_code != 200
    assert outcome.status_code == 403
    assert isinstance(outcome.json, Dict)
    assert 'msg' in outcome.json
    assert outcome.json.get('msg') == 'Invalid secret key!'


@pytest.mark.parametrize(
    'credentials,payload,headers_template',
    [(DATA_CREDENTIALS, DATA_PAYLOAD, DATA_HEADERS,)]
)
def test_devops_endpoint_jwt_used_more_than_once(
    client,
    credentials: Mapping,
    payload: Mapping,
    headers_template: Mapping
):
    rsp = client.post('/auth/login', json=credentials)
    headers = headers_template.copy()
    headers['X-JWT-KWY'] = rsp.json.get('access_token')
    first_attempt = client.post(
        '/DevOps', json=payload, headers=headers
    )
    second_attempt = client.post(
        '/DevOps', json=payload, headers=headers
    )
    assert first_attempt.status_code == 200
    assert second_attempt.status_code != 200
    assert second_attempt.status_code == 401
    assert isinstance(second_attempt.json, Dict)
    assert 'msg' in second_attempt.json
    assert second_attempt.json.get('msg') == 'Token has been revoked'


@pytest.mark.parametrize(
    'credentials,headers_template',
    [(DATA_CREDENTIALS, DATA_HEADERS,)]
)
def test_logout_endpoint(
    client,
    credentials: Mapping,
    headers_template: Mapping
):
    rsp = client.post('/auth/login', json=credentials)
    headers = headers_template.copy()
    headers['X-JWT-KWY'] = rsp.json.get('access_token')
    outcome = client.delete('/auth/logout', headers=headers)
    assert outcome.status_code == 200
    assert isinstance(outcome.json, Dict)
    assert 'msg' in outcome.json
    assert outcome.json.get('msg') == 'Access token revoked'
