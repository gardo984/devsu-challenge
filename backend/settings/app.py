#!/usr/bin/env python3

import os
import redis
from typing import Dict
from datetime import timedelta
from flask import (
    Flask,
    request,
    jsonify,
)
from flask_restful import (
    Api,
    Resource,
)
from flask_jwt_extended import (
    create_access_token,
    jwt_required,
    JWTManager,
    get_jwt,
)

app = Flask(__name__)
api = Api(app)

SECRET_KEY = "2f5ae96c-b558-4c7b-a590-a501ae1c3f6c"
app.config["JWT_SECRET_KEY"] = SECRET_KEY
app.config["JWT_HEADER_NAME"] = "X-JWT-KWY"
app.config["JWT_HEADER_TYPE"] = ""
jwt = JWTManager(app)

jwt_redis_blocklist = redis.StrictRedis(
    host=os.environ.get("BROKER_HOST", "localhost"),
    port=os.environ.get("BROKER_PORT", "6379"),
    db=0,
    decode_responses=True
)


def jwt_validate_secret_key():
    """ Validate if secret key is correct or not """
    def wrapper(fn):
        def decorator(*args, **kwargs):
            req_secret_key = request.headers.get("X-Parse-Rest-Api-Key")
            if SECRET_KEY == req_secret_key:
                return fn(*args, **kwargs)
            else:
                return {"msg": "Invalid secret key!"}, 403
        return decorator
    return wrapper


def jwt_block_used_token():
    """ Block jwt once it is been used """
    def wrapper(fn):
        def decorator(*args, **kwargs):
            output = fn(*args, **kwargs)
            jti = get_jwt().get('jti')
            jwt_redis_blocklist.set(jti, "")
            return output
        return decorator
    return wrapper


@jwt.token_in_blocklist_loader
def check_if_token_is_revoked(jwt_header, jwt_payload: dict):
    jti = jwt_payload["jti"]
    token_in_redis = jwt_redis_blocklist.get(jti)
    return token_in_redis is not None


class Login(Resource):
    """
    Get JWT based on the right credentials
    """

    def post(self):
        username = request.json.get("username", None)
        password = request.json.get("password", None)
        if username != "devsu" or password != "test":
            return jsonify({"msg": "Bad username or password"}), 401

        access_token = create_access_token(
            identity=username,
            fresh=timedelta(seconds=50),
        )
        return jsonify(access_token=access_token)


class LogOut(Resource):
    """ Block access token """

    @jwt_required(locations=['headers', ])
    def delete(self):
        jti = get_jwt()["jti"]
        jwt_redis_blocklist.set(jti, "")
        return jsonify(msg="Access token revoked")


class HelloWorld(Resource):
    """
    Return a hello world message based on the
    payload sent by the front
    """

    @jwt_required(fresh=True, locations=['headers', ])
    @jwt_validate_secret_key()
    @jwt_block_used_token()
    def post(self):
        data: Dict = request.json
        fullname = data.get('to')
        msg: str = (
            f"Hello {fullname} your message will be send"
        )
        return {'message': msg, }


api.add_resource(Login, "/auth/login")
api.add_resource(LogOut, "/auth/logout")
api.add_resource(HelloWorld, "/DevOps")
