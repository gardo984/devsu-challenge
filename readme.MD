# Devsu Code Challenge

## Setup Application

> ### Important!
> For development purposes set your `UID` in the file `docker/docker-compose.yml` with the output of the command `id -u <your-account>`. 
>
- Setup containers with the flag `--build`:
```sh
$ docker-compose -f docker/docker-compose.yml up --build -d
```
- Check status:
```sh
$ docker-compose -f docker/docker-compose.yml ps
```

## Usage

- To get a jwt, perform the following command:
```sh
$ curl --location \
	 --request POST 'localhost:8001/auth/login' \
	 --header 'Content-Type: application/json' \
	 -d '{"username": "devsu","password": "test"}'
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6MTY1NjIwNDExNS4wNzY3OTEsImlhdCI6MTY1NjIwNDA2NSwianRpIjoiMzRmYjFlM2YtNTA5Mi00ZTk0LWIwYjQtMDJiYzk4OGEzNDc1IiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImRldnN1IiwibmJmIjoxNjU2MjA0MDY1LCJleHAiOjE2NTYyMDQ5NjV9.t1t9sWdl33cLAzM38XGzCBKH0Zq5_lS1LD4zpyXXX-0"
}
```
- To get the formatted message:
```sh
$ export SECRET="2f5ae96c-b558-4c7b-a590-a501ae1c3f6c"
$ export JWT="<jwt-generated>"
$ curl -X POST \
	-H "X-Parse-REST-API-Key: ${SECRET}" \
	-H "X-JWT-KWY: ${JWT}" \
	-H "Content-Type: application/json" \
	-d '{ "message" : "This is a test", "to": "Juan Perez", "from": "Rita Asturia", "timeToLifeSec" : 45 }' \
	http://localhost:8001/DevOps
{
    "message": "Hello Juan Perez your message will be send"
}
```
- Outputs:
	- Invalid secret key:
	```sh
	{
	  "msg": "Invalid secret key!"
	}
	```
	- When jwt was used in a transaction:
	```sh
	{
	  "msg": "Token has been revoked"
	}
	```

## Unit Tests

- To perform unit tests:
```sh
docker-compose -f docker/docker-compose.yml up -d --build
docker-compose -f docker/docker-compose.yml exec devsu_backend python -m pytest -v
```

## Notes

- If you are building images from `Mac OS`, perform the following command:
```sh
docker build -f backend/Dockerfile --build-arg="UID=1000" --platform=linux/amd64 -t malazo/devsu-backend:latest-amd64 .
docker push malazo/devsu-backend:latest-amd64
```
- From linux:
```sh
docker-compose -f docker/docker-compose.yml build --no-cache devsu_backend
docker push malazo/devsu-backend:latest
```
